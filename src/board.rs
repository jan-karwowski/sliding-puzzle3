ub mod board;

extern crate rand;
use rand::seq::SliceRandom;
use rand::Rng;

pub enum Dir {
    Top,
    Bottom,
    Left,
    Right,
}

impl Dir {
    pub fn offset(&self) -> (i16, i16) {
        match self {
            Dir::Top => (0, -1),
            Dir::Bottom => (0, 1),
            Dir::Left => (-1, 0),
            Dir::Right => (1, 0),
        }
    }
}

pub struct SlidingBoard {
    size: u16,
    values: Vec<u16>,
}

impl SlidingBoard {
    pub fn board_init(size: u16) -> SlidingBoard {
        let mut ret = SlidingBoard {
            size: size,
            values: Vec::default(),
        };

        for i in 1..(size * size + 1) {
            ret.values.push(i % (size * size))
        }

        ret
    }

    fn shift_point(&self, pt: &(u16, u16), d: &(i16, i16)) -> Option<(u16, u16)> {
        let sum = ((pt.0 as i16) + d.0, (pt.1 as i16) + d.1);
        if sum.0 >= 0 && sum.1 >= 0 && sum.0 < (self.size as i16) && sum.1 < (self.size as i16) {
            Some((sum.0 as u16, sum.1 as u16))
        } else {
            None
        }
    }

    pub fn shuffle<R: Rng>(&mut self, rng: &mut R) -> () {
        self.values.shuffle(rng);
    }
    pub fn is_solved(&self) -> bool {
        let mut i = 0;
        while i < (self.size * self.size)
            && self.values[i as usize] == (i + 1) % (self.size * self.size)
        {
            i = i + 1;
        }
        i == self.size * self.size
    }

    fn idx_to_coords(&self, idx: usize) -> (u16, u16) {
        (idx as u16 % self.size, idx as u16 / self.size)
    }

    fn coord_to_index(&self, crd: &(u16, u16)) -> usize {
        (crd.0 * self.size + crd.1) as usize
    }

    pub fn zero_pos(&self) -> usize {
        let mut i = 0;
        while i < self.values.len() && self.values[i] != 0 {
            i = i + 1;
        }
        i
    }

    pub fn make_move(&mut self, dir: Dir) -> bool {
        let pos = self.zero_pos();
        match self
            .shift_point(&self.idx_to_coords(pos), &dir.offset())
            .map(|p| self.coord_to_index(&p))
        {
            Some(p) => {
                self.values.swap(pos, p);
                true
            }
            None => false,
        }
    }
}


