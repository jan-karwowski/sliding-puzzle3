extern crate rand;
use rand::thread_rng;
use sliding_puzzle3::board::*;
use std::clone::Clone;
use std::collections::HashSet;
use std::vec::Vec;

fn solve(
    sb: &mut SlidingBoard,
    path: &mut Vec<Dir>,
    max_depth: usize,
    visited_states: &mut HashSet<SlidingBoard>,
) -> Option<Vec<Dir>> {
    if sb.is_solved() {
        Some(path.clone())
    } else if path.len() >= max_depth {
        None
    } else {
        try_direction(sb, path, max_depth, visited_states, Dir::Top)
            .or_else(|| try_direction(sb, path, max_depth, visited_states, Dir::Bottom))
            .or_else(|| try_direction(sb, path, max_depth, visited_states, Dir::Left))
            .or_else(|| try_direction(sb, path, max_depth, visited_states, Dir::Right))
    }
}

fn try_direction(
    sb: &mut SlidingBoard,
    path: &mut Vec<Dir>,
    max_depth: usize,
    visited_states: &mut HashSet<SlidingBoard>,
    dir: Dir,
) -> Option<Vec<Dir>> {
    if sb.make_move(dir) {
        let ret = if visited_states.contains(sb) {
            None
        } else {
            visited_states.insert(sb.clone());
            path.push(dir);
            let x = solve(sb, path, max_depth, visited_states);
            path.pop();
            x
        };
        sb.make_move(dir.opposite());
        ret
    } else {
        None
    }
}

fn main() {
    let mut rng = thread_rng();
    let mut sb = SlidingBoard::board_init(4);
    sb.shuffle(&mut rng);
    let mut visited = HashSet::new();
    visited.insert(sb.clone());
    println!(
        "Result {:?} {:?}",
        solve(&mut sb, &mut Vec::new(), 30, &mut visited),
        visited.len()
    );
}
